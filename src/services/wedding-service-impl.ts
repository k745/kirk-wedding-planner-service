import { Wedding, Expense } from "../entities";
import { ExpenseDAO, WeddingDAO } from "./daos/wedding-dao";
import { WeddingDaoPostgres } from "./daos/wedding-dao-postgres";
import WeddingService from "./wedding-service";



export class WeddingServiceImpl implements WeddingService{
    

    weddingDAO:WeddingDAO = new WeddingDaoPostgres();
    expenseDAO:ExpenseDAO = new WeddingDaoPostgres();


    registerWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }
    registerExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.createExpense(expense);
    }
    retrieveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }
    retrieveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }
    retrieveAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }
    retrieveAllExpensesById(weddingId: number): Promise<Expense[]> {
        return this.expenseDAO.getAllExpensesById(weddingId);
    }
    retrieveExpenseById(expenseId: number): Promise<Expense> {
        return this.expenseDAO.getExpenseById(expenseId);
    }
    modifyWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding);
    }
    modifyExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.updateExpense(expense);
    }
    removeWedding(weddingId: number): Promise<boolean> {
        return this.weddingDAO.deleteWeddingById(weddingId);
    }
    removeExpense(expenseId: number): Promise<boolean> {
        return this.expenseDAO.deleteExpenseById(expenseId)
    }
    
}