import { client } from "../../connection";
import { Wedding } from "../../entities";
import { Expense } from "../../entities";
import { CreateDataError, MissingResourceError } from "../../errors";
import { ExpenseDAO, WeddingDAO } from "./wedding-dao";



export class WeddingDaoPostgres implements WeddingDAO, ExpenseDAO{
    

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = "SELECT * FROM wedding";
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        if(result.rowCount === 0){
            throw new MissingResourceError(`No weddings found`);
        }
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.wed_id,
                row.wed_date,
                row.wed_location,
                row.wed_name,
                row.wed_budget);
            weddings.push(wedding);
        }
        return weddings;
    }

    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = "SELECT * FROM wedding WHERE wed_id=$1";
        const values = [weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with ID ${weddingId} could not be found.`)
        }
        const row = result.rows[0]
        const wedding:Wedding = new Wedding(
            row.wed_id,
            row.wed_date,
            row.wed_location,
            row.wed_name,
            row.wed_budget);
        return wedding;
    }

    async getAllExpensesById(weddingId: number): Promise<Expense[]> {
        const sql:string = "SELECT * FROM expense WHERE wed_id=$1";
        const values = [weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expenses associated with wedding:${weddingId} could not be found.`)
        }
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.exp_id,
                row.wed_id,
                row.exp_reason,
                row.exp_amount);
            expenses.push(expense);
        }
        return expenses;
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = "SELECT * FROM expense";
        const result = await client.query(sql);
        const expenses:Expense[] = [];
        if(result.rowCount === 0){
            throw new MissingResourceError(`No expenses found.`)
        }
        const row = result.rows[0];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.exp_id,
                row.wed_id,
                row.exp_reason,
                row.exp_amount);
            expenses.push(expense);
        }
        return expenses;
    }

    async getExpenseById(expenseId: number): Promise<Expense> {
        const sql:string = "SELECT * FROM expense WHERE exp_id=$1";
        const values = [expenseId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with Id ${expenseId} could not be found.`)
        }
        const row = result.rows[0];
        const expense:Expense = new Expense(
            row.exp_id,
            row.wed_id,
            row.exp_reason,
            row.exp_amount
        );
        return expense;
    }

    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "INSERT INTO wedding (wed_id, wed_date, wed_location, wed_name, wed_budget) VALUES ($1,$2,$3,$4,$5)";
        const values = [wedding.weddingId,wedding.date,wedding.location,wedding.name,wedding.budget];
        await client.query(sql, values); //to do add error handler
        return wedding;
    }

    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "INSERT INTO expense (exp_id,wed_id,exp_reason,exp_amount) VALUES ($1,$2,$3,$4)";
        const values = [expense.expenseId,expense.weddingId,expense.reason,expense.amount];
        await client.query(sql, values); //to do add error handler
        return expense;
    }

    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "UPDATE wedding SET wed_date=$1, wed_location=$2, wed_name=$3, wed_budget=$4 WHERE wed_id=$5";
        const values = [wedding.date,wedding.location,wedding.name,wedding.budget,wedding.weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with Id ${wedding.weddingId} could not be found.`)
        }
        return wedding;
    }

    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = "UPDATE expense SET wed_id=$1,exp_reason=$2,exp_amount=$3 WHERE exp_id=$4";
        const values = [expense.weddingId,expense.reason,expense.amount,expense.expenseId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with Id ${expense.expenseId} could not be found`)
        }
        return expense;
    }

    async deleteWeddingById(weddingId: number): Promise<boolean> {
        const sql:string = "DELETE FROM wedding WHERE wed_id=$1";
        const values = [weddingId];
        const result = await client.query(sql, values)
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with Id of ${weddingId} could not be found.`);
        }
        return true;
    }

    async deleteExpenseById(expenseId: number): Promise<boolean> {
        const sql:string = "DELETE FROM expense WHERE exp_id=$1";
        const values = [expenseId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with Id ${expenseId} could not be found`)
        }
        return true;
    } 
}