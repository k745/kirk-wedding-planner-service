import express from 'express';
import { Expense, Wedding } from './entities';
import { CreateDataError, MissingResourceError } from './errors';
import WeddingService from './services/wedding-service';
import { WeddingServiceImpl } from './services/wedding-service-impl';
import cors from 'cors';

const app = express();
app.use(express.json());
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();

app.get("/weddings", async (req, res) =>{
    try{
        const weddings:Wedding[] = await weddingService.retrieveAllWeddings();
        res.status(200);
        res.send(weddings);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
    
});

app.get("/weddings/:id", async (req, res) =>{

    try{
        const weddingId = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingById(weddingId);
        res.status(200);
        res.send(wedding);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/weddings/:id/expenses", async (req, res) =>{

    try{
        const weddingId = Number(req.params.id);
        const expense:Expense[] = await weddingService.retrieveAllExpensesById(weddingId);
        res.status(200);
        res.send(expense);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/expenses", async (req, res) =>{

    try{
        const expenses:Expense[] = await weddingService.retrieveAllExpenses();
        res.status(200);
        res.send(expenses);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/expenses/:id", async (req, res) =>{

    try{
        const expenseId = Number(req.params.id);
        const expense:Expense = await weddingService.retrieveExpenseById(expenseId);
        res.status(200);
        res.send(expense);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.post("/weddings", async (req, res) =>{

    try{
        let wedding:Wedding = req.body;
        wedding = await weddingService.registerWedding(wedding);
        res.status(201);
        res.send(wedding);
    }catch(error){
        if(error instanceof CreateDataError){ //to do implement method
            res.status(404);
            res.send(error);
        }
    }
});

app.post("/expenses", async (req, res) =>{

    try{
        let expense:Expense = req.body;
        expense = await weddingService.registerExpense(expense);
        res.status(201);
        res.send(expense);
    }catch(error){
        if(error instanceof CreateDataError){ //to do implement method
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/weddings", async (req, res) =>{

    try{
        let wedding:Wedding = req.body;
        wedding = await weddingService.modifyWedding(wedding);
        res.status(200);
        res.send(wedding);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/expenses", async (req, res) =>{

    try{
        let expense:Expense = req.body;
        expense = await weddingService.modifyExpense(expense);
        res.status(200);
        res.send(expense);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.delete("/weddings/:id", async (req, res) =>{

    try{
        const weddingId = Number(req.params.id);
        const result = await weddingService.removeWedding(weddingId);
        res.status(200);
        res.send(result);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.delete("/expenses/:id", async (req, res) =>{

    try{
        const expenseId = Number(req.params.id);
        const result = await weddingService.removeExpense(expenseId);
        res.status(200);
        res.send(result);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Application Started on port ${PORT}`));