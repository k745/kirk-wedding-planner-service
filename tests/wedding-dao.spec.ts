import { Expense } from "../src/entities";
import { Wedding } from "../src/entities";
import { client } from "../src/connection";
import WeddingService from "../src/services/wedding-service";
import { WeddingServiceImpl } from "../src/services/wedding-service-impl";

const wedddingService:WeddingService = new WeddingServiceImpl();

test("Create a wedding", async () =>{
    const testWedding:Wedding = new Wedding(99,"2022-02-02","test location","test name",9999);
    const result:Wedding = await wedddingService.registerWedding(testWedding);
    expect(result.weddingId).toBe(99);
});

test("Create an expense", async () =>{
    const testExpense:Expense = new Expense(100,99,"test reason",100);
    const result:Expense = await wedddingService.registerExpense(testExpense);
    expect(result.expenseId).toBe(100);
});

test("Edit a wedding", async () =>{
    const editWedding:Wedding = new Wedding(99,"2023-03-03","edit wedding","edit name",1000);
    const result = await wedddingService.modifyWedding(editWedding);
    expect(result.budget).toBe(1000);
});

test("Edit an expense", async () =>{
    const editExpense:Expense = new Expense(100,99,"edit reason",200);
    const result:Expense = await wedddingService.modifyExpense(editExpense);
    expect(result.amount).toBe(200);
});

test("Get wedding by Id", async () =>{
    const weddingId = 99;
    const result = await wedddingService.retrieveWeddingById(weddingId);
    expect(result.budget).toBe(1000);
});

test("Get expense by Id", async () =>{
    const expenseId = 100;
    const result = await wedddingService.retrieveExpenseById(expenseId);
    expect(result.weddingId).toBe(99);
});

test("Get all weddings", async () =>{
    const result = await wedddingService.retrieveAllWeddings();
    expect(result).not.toBe(null);
});

test("Get all expenses by wedding Id", async () =>{
    const weddingId = 99;
    const result = await wedddingService.retrieveAllExpensesById(weddingId);
    expect(result).not.toBe(null);
});

test("Get all expenses", async () =>{
    const result = await wedddingService.retrieveAllExpenses();
    expect(result).not.toBe(null);
});

test("Delete expense by Id", async () =>{
    const expenseId = 100;
    const result = await wedddingService.removeExpense(expenseId);
    expect(result).toBe(true);
});

test("Delete wedding by Id", async () =>{
    const weddingId = 99;
    const result = await wedddingService.removeWedding(weddingId);
    expect(result).toBe(true);
});

afterAll(()=>{
    client.end();
});