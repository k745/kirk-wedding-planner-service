"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.WeddingDaoPostgres = void 0;
var connection_1 = require("../../connection");
var entities_1 = require("../../entities");
var entities_2 = require("../../entities");
var errors_1 = require("../../errors");
var WeddingDaoPostgres = /** @class */ (function () {
    function WeddingDaoPostgres() {
    }
    WeddingDaoPostgres.prototype.getAllWeddings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result, weddings, _i, _a, row, wedding;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sql = "SELECT * FROM wedding";
                        return [4 /*yield*/, connection_1.client.query(sql)];
                    case 1:
                        result = _b.sent();
                        weddings = [];
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("No weddings found");
                        }
                        for (_i = 0, _a = result.rows; _i < _a.length; _i++) {
                            row = _a[_i];
                            wedding = new entities_1.Wedding(row.wed_id, row.wed_date, row.wed_location, row.wed_name, row.wed_budget);
                            weddings.push(wedding);
                        }
                        return [2 /*return*/, weddings];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.getWeddingById = function (weddingId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, row, wedding;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "SELECT * FROM wedding WHERE wed_id=$1";
                        values = [weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The wedding with ID " + weddingId + " could not be found.");
                        }
                        row = result.rows[0];
                        wedding = new entities_1.Wedding(row.wed_id, row.wed_date, row.wed_location, row.wed_name, row.wed_budget);
                        return [2 /*return*/, wedding];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.getAllExpensesById = function (weddingId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, expenses, _i, _a, row, expense;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sql = "SELECT * FROM expense WHERE wed_id=$1";
                        values = [weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _b.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expenses associated with wedding:" + weddingId + " could not be found.");
                        }
                        expenses = [];
                        for (_i = 0, _a = result.rows; _i < _a.length; _i++) {
                            row = _a[_i];
                            expense = new entities_2.Expense(row.exp_id, row.wed_id, row.exp_reason, row.exp_amount);
                            expenses.push(expense);
                        }
                        return [2 /*return*/, expenses];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.getAllExpenses = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result, expenses, row, _i, _a, row_1, expense;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sql = "SELECT * FROM expense";
                        return [4 /*yield*/, connection_1.client.query(sql)];
                    case 1:
                        result = _b.sent();
                        expenses = [];
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("No expenses found.");
                        }
                        row = result.rows[0];
                        for (_i = 0, _a = result.rows; _i < _a.length; _i++) {
                            row_1 = _a[_i];
                            expense = new entities_2.Expense(row_1.exp_id, row_1.wed_id, row_1.exp_reason, row_1.exp_amount);
                            expenses.push(expense);
                        }
                        return [2 /*return*/, expenses];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.getExpenseById = function (expenseId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, row, expense;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "SELECT * FROM expense WHERE exp_id=$1";
                        values = [expenseId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expense with Id " + expenseId + " could not be found.");
                        }
                        row = result.rows[0];
                        expense = new entities_2.Expense(row.exp_id, row.wed_id, row.exp_reason, row.exp_amount);
                        return [2 /*return*/, expense];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.createWedding = function (wedding) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "INSERT INTO wedding (wed_id, wed_date, wed_location, wed_name, wed_budget) VALUES ($1,$2,$3,$4,$5)";
                        values = [wedding.weddingId, wedding.date, wedding.location, wedding.name, wedding.budget];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        _a.sent(); //to do add error handler
                        return [2 /*return*/, wedding];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.createExpense = function (expense) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "INSERT INTO expense (exp_id,wed_id,exp_reason,exp_amount) VALUES ($1,$2,$3,$4)";
                        values = [expense.expenseId, expense.weddingId, expense.reason, expense.amount];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        _a.sent(); //to do add error handler
                        return [2 /*return*/, expense];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.updateWedding = function (wedding) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "UPDATE wedding SET wed_date=$1, wed_location=$2, wed_name=$3, wed_budget=$4 WHERE wed_id=$5";
                        values = [wedding.date, wedding.location, wedding.name, wedding.budget, wedding.weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The wedding with Id " + wedding.weddingId + " could not be found.");
                        }
                        return [2 /*return*/, wedding];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.updateExpense = function (expense) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "UPDATE expense SET wed_id=$1,exp_reason=$2,exp_amount=$3 WHERE exp_id=$4";
                        values = [expense.weddingId, expense.reason, expense.amount, expense.expenseId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expense with Id " + expense.expenseId + " could not be found");
                        }
                        return [2 /*return*/, expense];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.deleteWeddingById = function (weddingId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "DELETE FROM wedding WHERE wed_id=$1";
                        values = [weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The wedding with Id of " + weddingId + " could not be found.");
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    WeddingDaoPostgres.prototype.deleteExpenseById = function (expenseId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "DELETE FROM expense WHERE exp_id=$1";
                        values = [expenseId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expense with Id " + expenseId + " could not be found");
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return WeddingDaoPostgres;
}());
exports.WeddingDaoPostgres = WeddingDaoPostgres;
