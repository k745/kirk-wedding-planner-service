"use strict";
exports.__esModule = true;
exports.CreateDataError = exports.MissingResourceError = void 0;
var MissingResourceError = /** @class */ (function () {
    function MissingResourceError(message) {
        this.description = "Sorry, the resource you requested could not be found";
        this.message = message;
    }
    return MissingResourceError;
}());
exports.MissingResourceError = MissingResourceError;
var CreateDataError = /** @class */ (function () {
    function CreateDataError(message) {
        this.description = "Sorry, there was an error creating the data you requested";
        this.message = message;
    }
    return CreateDataError;
}());
exports.CreateDataError = CreateDataError;
