"use strict";
exports.__esModule = true;
exports.client = void 0;
var pg_1 = require("pg");
require('dotenv').config({ path: 'C:\\Users\\Mitchell\\Desktop\\WeddingPlanner\\app.env' });
exports.client = new pg_1.Client({
    user: 'postgres',
    password: process.env.BANKINGAPIPW,
    database: process.env.DATABASENAME,
    port: 5432,
    host: '34.139.115.50'
});
exports.client.connect();
